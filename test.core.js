
const expect = chai.expect;

describe('Grid', function() {

  describe('when collapsing right', function() {

    it('an empty grid should remain empty', function() {
      const grid = Grid.createEmpty();
      const out = Grid.collapseRight(grid);
      expect(out).to.deep.equal(grid);
    });

    it('different values should remain unchanged', function() {
      const grid = [
        [2, 4, 1, 8],
        [8, 4, 2, 1],
        [1, 2, 4, 8],
        [64, 16, 1024, 128]];
      const out = Grid.collapseRight(grid);
      expect(out[3]).to.deep.equal(grid[3]);
    });

    it('same item not next to each other should remain unchanged', function() {
      const grid = [
        [2, 4, 2, 4],
        [4, 2, 4, 2],
        [2, 4, 2, 4],
        [4, 2, 4, 2]];
      const out = Grid.collapseRight(grid);
      expect(out).to.deep.equal(grid);
    });

    it('spaces between different items should be collapsed', function() {
      const grid = [
        [0, 2, 0, 4],
        [2, 0, 0, 4],
        [2, 4, 0, 0],
        [4, 0, 0, 0]];
      const out = Grid.collapseRight(grid);

      const expected = [
        [0, 0, 2, 4],
        [0, 0, 2, 4],
        [0, 0, 2, 4],
        [0, 0, 0, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should be collapsed ignoring empty spaces', function() {
      const grid = [
        [0, 2, 0, 2],
        [2, 0, 0, 2],
        [2, 2, 0, 0],
        [0, 2, 2, 0]];
      const out = Grid.collapseRight(grid);

      const expected = [
        [0, 0, 0, 4],
        [0, 0, 0, 4],
        [0, 0, 0, 4],
        [0, 0, 0, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should collapse from the far end first', function() {
      const grid = [
        [0, 2, 2, 2],
        [2, 0, 2, 2],
        [2, 2, 0, 2],
        [2, 2, 2, 0]];
      const out = Grid.collapseRight(grid);

      const expected = [
        [0, 0, 2, 4],
        [0, 0, 2, 4],
        [0, 0, 2, 4],
        [0, 0, 2, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('multiple matching items should collapse', function() {
      const grid = [
        [4, 4, 2, 2], //this shouldn't double colapse the 2 result
        [4, 4, 8, 8],
        [16, 16, 32, 32],
        [128, 128, 1024, 1024]];
      const out = Grid.collapseRight(grid);

      const expected = [
        [0, 0, 8, 4],
        [0, 0, 8, 16],
        [0, 0, 32, 64],
        [0, 0, 256, 2048]];
      expect(out).to.deep.equal(expected);
    });

  });

  describe('when collapsing left', function() {

    it('an empty grid should remain empty', function() {
      const grid = Grid.createEmpty();
      const out = Grid.collapseLeft(grid);
      expect(out).to.deep.equal(grid);
    });

    it('different values should remain unchanged', function() {
      const grid = [
        [2, 4, 1, 8],
        [8, 4, 2, 1],
        [1, 2, 4, 8],
        [64, 16, 1024, 128]];
      const out = Grid.collapseLeft(grid);
      expect(out[3]).to.deep.equal(grid[3]);
    });

    it('same item not next to each other should remain unchanged', function() {
      const grid = [
        [2, 4, 2, 4],
        [4, 2, 4, 2],
        [2, 4, 2, 4],
        [4, 2, 4, 2]];
      const out = Grid.collapseLeft(grid);
      expect(out).to.deep.equal(grid);
    });

    it('spaces between different items should be collapsed', function() {
      const grid = [
        [0, 2, 0, 4],
        [2, 0, 0, 4],
        [2, 4, 0, 0],
        [4, 0, 0, 0]];
      const out = Grid.collapseLeft(grid);

      const expected = [
        [2, 4, 0, 0],
        [2, 4, 0, 0],
        [2, 4, 0, 0],
        [4, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should be collapsed ignoring empty spaces', function() {
      const grid = [
        [0, 2, 0, 2],
        [2, 0, 0, 2],
        [2, 2, 0, 0],
        [0, 2, 2, 0]];
      const out = Grid.collapseLeft(grid);

      const expected = [
        [4, 0, 0, 0],
        [4, 0, 0, 0],
        [4, 0, 0, 0],
        [4, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should collapse from the far end first', function() {
      const grid = [
        [0, 2, 2, 2],
        [2, 0, 2, 2],
        [2, 2, 0, 2],
        [2, 2, 2, 0]];
      const out = Grid.collapseLeft(grid);

      const expected = [
        [4, 2, 0, 0],
        [4, 2, 0, 0],
        [4, 2, 0, 0],
        [4, 2, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('multiple matching items should collapse', function() {
      const grid = [
        [4, 4, 2, 2], //this shouldn't double colapse the 2 result
        [4, 4, 8, 8],
        [16, 16, 32, 32],
        [128, 128, 1024, 1024]];
      const out = Grid.collapseLeft(grid);

      const expected = [
        [8, 4, 0, 0],
        [8, 16, 0, 0],
        [32, 64, 0, 0],
        [256, 2048, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

  });

  describe('when collapsing up', function() {

    it('an empty grid should remain empty', function() {
      const grid = Grid.createEmpty();
      const out = Grid.collapseUp(grid);
      expect(out).to.deep.equal(grid);
    });

    it('different values should remain unchanged', function() {
      const grid = [
        [2, 4, 1, 8],
        [8, 8, 2, 1],
        [1, 2, 4, 4],
        [4, 1, 8, 2]];
      const out = Grid.collapseUp(grid);
      expect(out[3]).to.deep.equal(grid[3]);
    });

    it('same item not next to each other should remain unchanged', function() {
      const grid = [
        [2, 4, 2, 4],
        [4, 2, 4, 2],
        [2, 4, 2, 4],
        [4, 2, 4, 2]];
      const out = Grid.collapseUp(grid);
      expect(out).to.deep.equal(grid);
    });

    it('spaces between different items should be collapsed', function() {
      const grid = [
        [4, 4, 0, 0],
        [0, 0, 0, 0],
        [2, 0, 4, 0],
        [0, 2, 2, 4]];
      const out = Grid.collapseUp(grid);

      const expected = [
        [4, 4, 4, 4],
        [2, 2, 2, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should be collapsed ignoring empty spaces', function() {
      const grid = [
        [2, 2, 0, 0],
        [0, 0, 0, 2],
        [2, 0, 2, 2],
        [0, 2, 2, 0]];
      const out = Grid.collapseUp(grid);

      const expected = [
        [4, 4, 4, 4],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should collapse from the far end first', function() {
      const grid = [
        [0, 2, 2, 2],
        [2, 0, 2, 2],
        [2, 2, 0, 2],
        [2, 2, 2, 0]];
      const out = Grid.collapseUp(grid);

      const expected = [
        [4, 4, 4, 4],
        [2, 2, 2, 2],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

    it('multiple matching items should collapse', function() {
      const grid = [
        [4, 4, 16, 128],
        [4, 4, 16, 128],
        [2, 8, 32, 1024],
        [2, 8, 32, 1024]];
      const out = Grid.collapseUp(grid);

      const expected = [
        [8, 8, 32, 256],
        [4, 16, 64, 2048],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];
      expect(out).to.deep.equal(expected);
    });

  });

  describe('when collapsing down', function() {

    it('an empty grid should remain empty', function() {
      const grid = Grid.createEmpty();
      const out = Grid.collapseDown(grid);
      expect(out).to.deep.equal(grid);
    });

    it('different values should remain unchanged', function() {
      const grid = [
        [2, 4, 1, 8],
        [8, 8, 2, 1],
        [1, 2, 4, 4],
        [4, 1, 8, 2]];
      const out = Grid.collapseDown(grid);
      expect(out[3]).to.deep.equal(grid[3]);
    });

    it('same item not next to each other should remain unchanged', function() {
      const grid = [
        [2, 4, 2, 4],
        [4, 2, 4, 2],
        [2, 4, 2, 4],
        [4, 2, 4, 2]];
      const out = Grid.collapseDown(grid);
      expect(out).to.deep.equal(grid);
    });

    it('spaces between different items should be collapsed', function() {
      const grid = [
        [4, 4, 0, 0],
        [0, 0, 0, 0],
        [2, 0, 4, 0],
        [0, 2, 2, 4]];
      const out = Grid.collapseDown(grid);

      const expected = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [4, 4, 4, 0],
        [2, 2, 2, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should be collapsed ignoring empty spaces', function() {
      const grid = [
        [2, 2, 0, 0],
        [0, 0, 0, 2],
        [2, 0, 2, 2],
        [0, 2, 2, 0]];
      const out = Grid.collapseDown(grid);

      const expected = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [4, 4, 4, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('matching items should collapse from the far end first', function() {
      const grid = [
        [0, 2, 2, 2],
        [2, 0, 2, 2],
        [2, 2, 0, 2],
        [2, 2, 2, 0]];
      const out = Grid.collapseDown(grid);

      const expected = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [2, 2, 2, 2],
        [4, 4, 4, 4]];
      expect(out).to.deep.equal(expected);
    });

    it('multiple matching items should collapse', function() {
      const grid = [
        [4, 4, 16, 128],
        [4, 4, 16, 128],
        [2, 8, 32, 1024],
        [2, 8, 32, 1024]];
      const out = Grid.collapseDown(grid);

      const expected = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [8, 8, 32, 256],
        [4, 16, 64, 2048]];
      expect(out).to.deep.equal(expected);
    });

  });

  describe('when checking for lost condition', function() {

    it('should lose when collapsing in any direction would have the original grid', function() {
      const grid = [
        [2, 4, 1, 8],
        [4, 8, 2, 1],
        [1, 2, 4, 8],
        [4, 1, 8, 2]];
      const out = Grid.hasLost(grid);
      expect(out).to.be.true;
    });

    it('should not lose when cells can combine vertically', function() {
      const grid = [
        [2, 4, 1, 8],
        [4, 8, 2, 1],
        [1, 8, 4, 8],
        [4, 1, 8, 2]];
      const out = Grid.hasLost(grid);
      expect(out).to.be.false;
    });

    it('should not lose when cells can combine horizontally', function() {
      const grid = [
        [2, 4, 1, 8],
        [4, 8, 2, 1],
        [1, 4, 4, 8],
        [4, 1, 8, 2]];
      const out = Grid.hasLost(grid);
      expect(out).to.be.false;
    });

    it('should not lose when there is an empty space', function() {
      const grid = [
        [2, 4, 1, 8],
        [4, 8, 2, 1],
        [1, 0, 4, 8],
        [4, 1, 8, 2]];
      const out = Grid.hasLost(grid);
      expect(out).to.be.false;
    });

    it('should not lose when there is an empty space but nothing collapsable', function() {
      const grid = [
        [0, 0, 0, 4],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0]];
      const out = Grid.hasLost(grid);
      expect(out).to.be.false;
    });
  });

  describe('when adding random', function() {

    it('should add to only remaining spot', function() {
      const grid = [
        [2, 4, 2, 0],
        [4, 8, 1, 1],
        [1, 2, 4, 8],
        [4, 1, 8, 2]];
      const out = Grid.addRandom(grid);
      expect(out[0][3]).to.equal(2);
    });

    it('should should change grid', function() {
      const grid = Grid.createEmpty();
      const out = Grid.addRandom(grid);
      expect(out).not.to.deep.equal(grid);
    });

  });

});
