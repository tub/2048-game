# README #

Implemented the modern classic "2048" game a few different ways for practice.
* jquery.html (table version)
* jquery.div.html (via divs)
* react.div.html (via divs)

The solutions lack any animation. It wasn't really an objective for this practice project though.