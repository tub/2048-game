
//Grid is a namespace for grid actions
const Grid = (function() {

  const GRID_WIDTH = 4;
  const RAND_VALUE = 2;

  return {
    createEmpty,
    collapseRight,
    collapseLeft,
    collapseUp,
    collapseDown,
    hasLost,
    addRandom,
    areGridsEqual,
    GRID_WIDTH
  };

  function createEmpty() {
    return [
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0],
      [0, 0, 0, 0]];

  }

  function hasLost(grid) {
    //game is lost if the grid cannot be collapsed any futher
    const collapsedGridRight = collapseRight(grid);
    const collapsedGridLeft = collapseLeft(grid);
    const collapsedGridUp = collapseUp(grid);
    const collapsedGridDown = collapseDown(grid);
    return areGridsEqual(grid, collapsedGridRight, collapsedGridLeft, collapsedGridUp, collapsedGridDown);
  }

  function addRandom(grid) {
    //get all the empty spaces as objects with x and y attributes
    var emptyCells = grid.reduce((acc, row, x) =>
      acc.concat(
        row.reduce((acc2, cellVal, y) =>
          cellVal === 0 ? acc2.concat({x: x, y: y}) : acc2
        , []))
    , []);

    //select a random empty cell
    var randEmptyCell = emptyCells[Math.floor(Math.random() * emptyCells.length)];

    return grid.map((row, x) =>
      row.map((val, y) =>
        x === randEmptyCell.x && y === randEmptyCell.y
         ? RAND_VALUE : val));
  }

  //if we wanted to make things faster, rather than transform each time,
  //provide different methods that collapse can use to itterate through the
  //same array

  function collapseRight(grid) {
    return grid.map(collapse); //no transform needed, matches exepcted format
  }

  function collapseLeft(grid) {
    const g = grid.map(x => x.slice().reverse()); //transform to so collapse() can use
    const out = g.map(collapse);
    return out.map(x => x.slice().reverse()); //transform back
  }

  function collapseUp(grid) {
    const g = rotatecw(grid); //transform to so collapse() can use
    const out = g.map(collapse);
    return rotateccw(out); //transform back
  }

  function collapseDown(grid) {
    const g = rotateccw(grid); //transform to so collapse() can use
    const out = g.map(collapse);
    return rotatecw(out); //transform back
  }

  function rotatecw(grid) {
    const mid = (GRID_WIDTH - 1) / 2;
    const out = createEmpty();
    for(let r = 0; r < GRID_WIDTH; r++) {
      for(let c = 0; c < GRID_WIDTH; c++) {
        out[c][GRID_WIDTH - 1 - r] = grid[r][c]
      }
    }
    return out;
  }

  function rotateccw(grid) {
    const out = createEmpty();
    for(let r = 0; r < GRID_WIDTH; r++) {
      for(let c = 0; c < GRID_WIDTH; c++) {
        out[GRID_WIDTH - 1 - c][r] = grid[r][c]
      }
    }
    return out;
  }

  function collapse(vec) {
    //collapse from the far side
    const outVec = [0, 0, 0, 0];

    let prevVal = null;
    let prevPos = null;
    let outPos = 3; //start from the end
    for (let i = GRID_WIDTH - 1; i >= 0; i--) {
      if (vec[i] !== 0) {
        if (prevVal === vec[i]) {
          outVec[prevPos] += vec[i];
          prevVal = null; //clear the prevVal
        } else {
          outVec[outPos] = vec[i];
          outPos--;
          prevVal = vec[i];
          prevPos = outPos + 1;
        }
      }
    }
    return outVec;
  }

  function areGridsEqual(origGrid, ...grids) {
    for(let g of grids) {
      for (let i = GRID_WIDTH - 1; i >= 0; i--) {
        for (let j = GRID_WIDTH - 1; j >= 0; j--) {
          if (origGrid[i][j] !== g[i][j]) {
            return false;
          }
        }
      }
    }
    return true;
  }

})();
